Nexus Vibe
=

![](./public/icon.jpg)

## _The social network that resonates._


[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Dillinger is a cloud-enabled, mobile-ready, offline-storage compatible,
AngularJS-powered HTML5 Markdown editor.

- Type some Markdown on the left
- See HTML in the right
- ✨Magic ✨

![](https://img.shields.io/github/stars/pandao/editor.md.svg) ![](https://img.shields.io/github/forks/pandao/editor.md.svg) ![](https://img.shields.io/github/tag/pandao/editor.md.svg) ![](https://img.shields.io/github/release/pandao/editor.md.svg) ![](https://img.shields.io/github/issues/pandao/editor.md.svg) ![](https://img.shields.io/bower/v/editor.md.svg)

## Features

- Import a HTML file and watch it magically convert to Markdown
- Drag and drop images (requires your Dropbox account be linked)
- Import and save files from GitHub, Dropbox, Google Drive and One Drive
- Drag and drop markdown and HTML files into Dillinger
- Export documents as Markdown, HTML and PDF

Markdown is a lightweight markup language based on the formatting conventions
that people naturally use in email.
As [John Gruber] writes on the [Markdown site][df1]

> The overriding design goal for Markdown's
> formatting syntax is to make it as readable
> as possible. The idea is that a
> Markdown-formatted document should be
> publishable as-is, as plain text, without
> looking like it's been marked up with tags
> or formatting instructions.

This text you see here is *actually- written in Markdown! To get a feel
for Markdown's syntax, type some text into the left window and
watch the results in the right.

## Tech

Dillinger uses a number of open source projects to work properly:

- [AngularJS] - HTML enhanced for web apps!
- [Ace Editor] - awesome web-based text editor
- [markdown-it] - Markdown parser done right. Fast and easy to extend.
- [Twitter Bootstrap] - great UI boilerplate for modern web apps
- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework [@tjholowaychuk]
- [Gulp] - the streaming build system
- [Breakdance](https://breakdance.github.io/breakdance/) - HTML
  to Markdown converter
- [jQuery] - duh

And of course Dillinger itself is open source with a [public repository][dill]
on GitHub.


### Installation
    ~# git clone github.com/mudassaralichouhan/nexus-vibe.git

### docker composer

    
    ~# docker-compose up

### Environment
* App
```dockerfile
app:
    build:
      context: .
      dockerfile: Dockerfile
    container_name: app-ctr
    ports:
      - "9000:9000"
    volumes:
      - .:/app
    networks:
      - clearly-network
    depends_on:
      - mongo
      - redis
      - minio
```
    
    # docker exec -it api_app bash
```dotenv
ENVIRMENT="development" # production
PORT="9000"
SECRET_KEY="WHCI2QWgEH2eEsKf8hSGVfEfcHJLwfqJ"
JWT_KEY="1:907944704427:web:24cd8cc05f653da2317aee"

# redis, jwt should be b/w
AUTH_DRIVER="redis"
```

* Mongo DB
```dockerfile
mongo:
    image: "prismagraphql/mongo-single-replica:5.0.3"
    container_name: mongo-ctr
    ports:
      - "27017:27017"
    environment:
      MONGO_INITDB_ROOT_USERNAME: $MONGODB_USER
      MONGO_INITDB_ROOT_PASSWORD: $MONGODB_PASSWORD
    volumes:
      - clearly_volume:/data/db
    networks:
      - clearly-network
```
    
    ~# docker exec -it mongo-ctr bash
    ~# mongosh --host <hostname>:<port> --username <your_username> --password <your_password> --authenticationDatabase admin
```dotenv
# database envirment
MONGODB_HOST="mongo"
MONGODB_DATABASE="nexus-vibe"
MONGODB_USER="PrSP92XoT8"
MONGODB_PASSWORD="V7UU40YNpg"
MONGODB_PORT="27017"
DATABASE_URL="mongodb://${MONGODB_USER}:${MONGODB_PASSWORD}@${MONGODB_HOST}:${MONGODB_PORT}/${MONGODB_DATABASE}?authSource=admin&directConnection=true"
```
* Redis
```dockerfile
redis:
    image: "redis:latest"
    container_name: redis-ctr
    ports:
      - "6379:6379"
    command: "redis-server --requirepass $REDIS_PASSWORD"
    volumes:
      - clearly_volume:/usr/local/etc/redis
    networks:
      - clearly-network
```
    
    ~# docker exec -it redis-ctr redis-cli
```dotenv
# caching redis envirment
REDIS_HOST="redis"
REDIS_PORT="6379"
REDIS_USERNAME="default"
REDIS_PASSWORD="YTXbGqPHmI"
```

* MinIO

```dockerfile

minio:
    image: quay.io/minio/minio
    container_name: minio-ctr
    ports:
      - "9001:9001"
    environment:
      - MINIO_ROOT_USER=$MINIO_ROOT_USER
      - MINIO_ROOT_PASSWORD=$MINIO_ROOT_PASS
    volumes:
      - clearly_volume:/data
    networks:
      - clearly-network
    command: server /data --console-address ":9001"
```

    ~# docker exec -it minio-ctr bash
```dotenv

# strage minio envirment
MINIO_HOST="minio"
MINIO_PORT="9000"
MINIO_ROOT_USER="root"
MINIO_ROOT_PASS=""
MINIO_ACCESS_KEY=""
MINIO_SECRET_KEY=""
MINIO_SSL=false
```

* mailhog

```dockerfile
mailhog:
    image: mailhog/mailhog
    container_name: mail-hog-ctr
    ports:
      - "1025:1025"  # SMTP port
      - "8025:8025"  # HTTP port
    #environment: # To run with authentication, uncomment the following lines
    #  - MH_UI_AUTH_FILE=/auth.htpasswd
    volumes:
      - clearly_volume:/app/data
    #  - ./path/to/auth.htpasswd:/auth.htpasswd
    networks:
      - clearly-network
```
    
    ~# docker exec -it mail-hog-ctr bash

```dotenv
# mail server envirment
MAIL_HOST="mailhog"
MAIL_PORT="1025"
MAIL_USER="5d0d4a05a12c04"
MAIL_PASS="2ffbc2cedc6154"
MAIL_FROM="no-replay@nexus-vibe.net"
```

### Prisma
* Seeder
> Package.json
```json
"prisma": {
    "seed": "ts-node prisma/seeder/seed.ts"
}
```
```json
"prisma-bind": "ts-node prisma/generator/schema.prisma.ts && prisma format",
"prisma-gen": "npx prisma generate",
"prisma-push": "npx prisma db push",
"prisma-seed": "npx prisma db seed",
```

    ~# npm run prisma-bind
    ~# npm run prisma-gen
    ~# npm run prisma-push
    ~# npm run prisma-seed
    npm run prisma-bind && npm run prisma-gen && npm run prisma-push



* User
> http://localhost:9000/api

* Admin 
> http://admin.localhost:9000/api


## License

MIT

**Free Software, Hell Yeah!**